#include<stdio.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<fcntl.h>
#include "hexd.h"


void hexdump(char *f){
    unsigned char BUF[BUFSIZE];
    int k=0;
    int bytesread;
    FILE *fp = fopen(f,"rb");
    //while(!feof(fp)){
    while(fread(&BUF,BUFSIZE,1,fp)){
        k=0;
        for(int i=0;i<BUFSIZE;i++){
            if(k==32){
                printf("\n");
                k=0;
            }
            if(BUF[i]!=0){
                printf("%s%02x ",KRED,BUF[i]);
                printf("%s",KWHT);
                k++;
            }
            else{
                printf("%02x ",BUF[i]);
                k++;
            }
        }
        memset(&BUF,BUFSIZE,0);

    }
    fclose(fp);
    printf("\n");
}
