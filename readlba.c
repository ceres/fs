#include<stdio.h>
#include "hexd.h"

int main(int argc,char **argv){
    if(argc < 2){
        printf("usage: %s %s\n",argv[0],"filename");
        return(-1);
    }
    char *flname = argv[1];
    hexdump(flname);
}
