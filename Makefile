all: readlba

readlba: hexd.o
	c99 -o readlba readlba.c hexd.o

hexd.o: hexd.c
	c99 -c hexd.c

.PHONY: clean all
clean:
	\rm *.o readlba
